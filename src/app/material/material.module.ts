import { NgModule } from '@angular/core';
import {MatButtonModule  } from '@angular/material/button'
// import {MatButtonModule , MatButtonToggleModule , MatIcon , MatToolbarModule} from '@angular/material/button'
// import {MatStepperModule,MatCardModule,MatTabsModule,MatExpansionModule,MatGridList, MaterialModule , MatListModule,MatDivideModule, MatProgressSpinnerModule , MatSidenavModule , MatMenuModule} from './material/material.module';

import {MatBadgeModule  } from '@angular/material/badge'
import { MatNativeDateModule } from '@angular/material/core';

const material = [
  MatButtonModule,
  // MaterialModule,
  // MatButtonToggleModule
  // MatIconModule
  // MatToolbarModule
  // MatSidenavModule,
  // MatProgressSpinnerModule,
  // MatMenuModule,
  // MatListModule,
  // MatDivideModule,
  // MatGridList,
  // MatExpansionModule,
  // MatCardModule,
  // MatTabsModule,
  // MatStepperModule
  // MatFormFieldModule,
  // MatInputModule,
  // MatSelectModule,
  // MatAutoCompleteModule
  // MatCheckBoxModule
  // MatRadioModule
  // MatDatePicker
  MatNativeDateModule,
  MatBadgeModule
]

@NgModule({
  imports: [material],
  exports:[material]
})
export class MaterialModule { }
